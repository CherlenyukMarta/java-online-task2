package com.epam;

import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        final int attempts = 100;
        Scanner sc = new Scanner(System.in);
        System.out.println("Please, enter how many people at the party: ");
        System.out.println("Number should be > 2");
        int n = sc.nextInt();
        int countTimesFullSpreaded = 0;
        int peopleReached = 0;
        for(int i=0;i<attempts;i++){
            boolean guests[] = new boolean[n];
            guests[1] = true; //Bob
            boolean alreadyHeard = false;
            int nextPerson = -1;
            int currentPerson = 1; //Bob
            while(!alreadyHeard){
                nextPerson = 1 + (int)(Math.random() * (n-1));
                if(nextPerson == currentPerson){
                    while(nextPerson == currentPerson)
                        nextPerson = 1 + (int)(Math.random() * (n-1));
                }
                if(guests[nextPerson]) //already heard
                {
                    if(rumorSpreaded(guests)) //all heard
                        countTimesFullSpreaded++;
                    peopleReached = peopleReached + countPeopleReached(guests); //guests at all
                    alreadyHeard = true;
                }
                guests[nextPerson] = true;
                currentPerson = nextPerson;
            }
        }
        System.out.println("Probability that everyone will hear rumor except Alice in "+attempts+" attempts: " +
                (double)countTimesFullSpreaded/attempts);
        System.out.println("Expected number of people to hear the rumor: "+peopleReached/attempts);
    }

    public static int countPeopleReached(boolean arr[]){
        int counter = 0;
        for(int i = 1;i<arr.length;i++)
            if(arr[i])
                counter++;
        return counter;
    }

    public static boolean rumorSpreaded(boolean arr[]){
        for(int i = 1;i<arr.length;i++)
            if(!arr[i])
                return false;
        return true;
    }


}
